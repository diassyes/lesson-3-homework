// Реализовать Bubble Sort для int'ов.
// Протестировать на массивах разной длины,
// при этом фиксировать затраченное на сортировку время.
// Определить временную сложность вашего алгоритма О(n)

package main

import (
	"fmt"
	"math"
	"math/rand"
	"time"
)

// Time measurement
func TimeTrack(start time.Time, length int) {
	elapsed := time.Since(start)
	fmt.Printf("Array length: %d\nTime elapsed: %s\n\n", length, elapsed)
}

// Sorting with Bubble-sort including time measurement function
func BubbleSort(arr *[]int){
	length := len(*arr)
	defer TimeTrack(time.Now(), length)
	for i := 0; i < length; i++ {
		for j := 0; j < length - i - 1; j++ {
			if (*arr)[j] > (*arr)[j + 1] {
				(*arr)[j], (*arr)[j + 1] = (*arr)[j + 1], (*arr)[j]
			}
		}
	}
}

func main() {
	for i := 0; i < 5; i++ {
		length := math.Pow(10, float64(i + 1))
		arr := rand.Perm(int(length))
		BubbleSort(&arr)
	}
}
/*
--- Time complexity ---

n-1 comparisons will be done in the 1st pass,
n-2 in 2nd pass,
n-3 in 3rd pass and so on.

So the total number of comparisons will be:
(n-1) + (n-2) + (n-3) + ..... + 3 + 2 + 1
Sum = n(n-1)/2

So time complexity of Bubble Sort is O(n2).

--- Time measurement ---

Array length: 10
Time elapsed: 826ns

Array length: 100
Time elapsed: 19.81µs

Array length: 1000
Time elapsed: 1.778811ms

Array length: 10000
Time elapsed: 196.617655ms

Array length: 100000
Time elapsed: 19.983380011s
*/