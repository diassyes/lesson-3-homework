// Реализовать колоду игральных карт.
// Карта должна иметь номинал и масть (пример: восьмерка треф)
// Также необходимо реализовать метод shuffle(),
// который будет перетасовывать вашу колоду карт.

package main

import (
	"fmt"
	"math/rand"
	"time"
)

type Suit string
type Rank string

// Array containing the names of suits
var SuitList []Suit = []Suit{
	"Clubs \U00002663",
	"Diamonds \U00002666",
	"Hearts \U00002665",
	"Spades \U00002660",
}

// Array containing the rank of cards
var RankList []Rank = []Rank{
	"Six",
	"Seven",
	"Eight",
	"Nine",
	"Ten",
	"Jack",
	"Queen",
	"King",
	"Ace",
}

// ----- A card -----
type Card struct {
	Suit Suit // suit - from SuitList
	Rank Rank // rank - from RankList
	Strength int // strength - from 0-8 depending on rank
}

// Prints the card information
func (card Card) PrintInfo() {
	fmt.Printf("%s  \t %s\n", card.Rank, card.Suit)
}

// A deck of 36 cards
type DeckOfCards struct {
	Cards [36]Card // 36-card array
}

// Fills a deck of cards with cards
func (doc *DeckOfCards) Fill() {
	for i, suit := range SuitList {
		for j, rank := range RankList {
			card := Card{suit, rank, j}
			doc.Cards[i*9 + j] = card
		}
	}
}

// Prints all cards from the deck
func (doc *DeckOfCards) PrintAll() {
	for i := 0; i < len(doc.Cards); i++ {
		fmt.Printf("%d] ", i + 1)
		doc.Cards[i].PrintInfo()
	}
}

// Shuffles randomly all the cards in the deck
func (doc *DeckOfCards) Shuffle() {
	rand.Seed(time.Now().UnixNano())
	for i := range doc.Cards {
		j := rand.Intn(i + 1)
		doc.Cards[i], doc.Cards[j] = doc.Cards[j], doc.Cards[i]
	}
}

func main() {
	myDeck := DeckOfCards{}
	myDeck.Fill()
	myDeck.PrintAll()
	myDeck.Shuffle()
	fmt.Println("\nAfter shuffling:\n")
	myDeck.PrintAll()
}


